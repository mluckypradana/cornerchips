<?php
// Heading
$_['heading_title']      = 'Akun Saya';

// Text
$_['text_account']       = 'Akun';
$_['text_my_account']    = 'Akun Saya';
$_['text_my_orders']     = 'Pesanan Saya';
$_['text_my_newsletter'] = 'Newsletter';
$_['text_edit']          = 'Ubah informasi akun anda';
$_['text_password']      = 'Ubah password anda';
$_['text_address']       = 'Ubah entry alamat anda';
$_['text_wishlist']      = 'Edit wish list anda';
$_['text_order']         = 'Lihat history pesanan';
$_['text_download']      = 'Downloads';
$_['text_reward']        = 'Point Reward Anda';
$_['text_return']        = 'Lihat request retur anda';
$_['text_transaction']   = 'Transaksi Anda';
$_['text_newsletter']    = 'Subscribe / unsubscribe newsletter';
$_['text_recurring']     = 'Pembayaran berulang';
$_['text_transactions']  = 'Transaksi';