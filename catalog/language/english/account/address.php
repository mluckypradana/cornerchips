<?php
// Heading
$_['heading_title']        = 'Alamat';

// Text
$_['text_account']         = 'Akun';
$_['text_address_book']    = 'Entry Alamat';
$_['text_edit_address']    = 'Ubah Alamat';
$_['text_add']             = 'Alamat berhasil ditambah';
$_['text_edit']            = 'Alamat berhasil diubah';
$_['text_delete']          = 'Alamat berhasil dihapus';
$_['text_empty']           = 'Tidak ada alamat di akun anda.';

// Entry
$_['entry_firstname']      = 'Nama Depan';
$_['entry_lastname']       = 'Nama Belakang';
$_['entry_company']        = 'Perusahaan';
$_['entry_address_1']      = 'Alamat 1';
$_['entry_address_2']      = 'Alamat 2';
$_['entry_postcode']       = 'Kode Pos';
$_['entry_city']           = 'Kota';
$_['entry_country']        = 'Negara';
$_['entry_zone']           = 'Region / State';
$_['entry_default']        = 'Alamat Default';

// Error
$_['error_delete']         = 'Peringatan: Anda harus mempunyai setidaknya satu alamat!';
$_['error_default']        = 'Warning: You can not delete your default address!';
$_['error_firstname']      = 'First Name must be between 1 and 32 characters!';
$_['error_lastname']       = 'Last Name must be between 1 and 32 characters!';
$_['error_vat']            = 'VAT number is invalid!';
$_['error_address_1']      = 'Address must be between 3 and 128 characters!';
$_['error_postcode']       = 'Postcode must be between 2 and 10 characters!';
$_['error_city']           = 'City must be between 2 and 128 characters!';
$_['error_country']        = 'Please select a country!';
$_['error_zone']           = 'Please select a region / state!';
$_['error_custom_field']   = '%s required!';
