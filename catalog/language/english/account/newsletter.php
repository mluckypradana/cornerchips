<?php
// Heading
$_['heading_title']    = 'Newsletter Subscription';

// Text
$_['text_account']     = 'Akun';
$_['text_newsletter']  = 'Newsletter';
$_['text_success']     = 'Sukses: Newsletter subscription anda telah berhasil diubah!';

// Entry
$_['entry_newsletter'] = 'Subscribe';
