<?php
// Text
$_['text_information']  = 'Informasi';
$_['text_service']      = 'Customer Service';
$_['text_extra']        = 'Extra';
$_['text_contact']      = 'Kontak Kami';
$_['text_return']       = 'Returns';
$_['text_sitemap']      = 'Peta Situs';
$_['text_manufacturer'] = 'Brand';
$_['text_voucher']      = 'Gift Vouchers';
$_['text_affiliate']    = 'Affiliates';
$_['text_special']      = 'Special';
$_['text_account']      = 'Akun Saya';
$_['text_order']        = 'History Pesanan';
$_['text_wishlist']     = 'Wish List';
$_['text_newsletter']   = 'Newsletter';
$_['text_powered']      = 'Powered By <a href="http://www.opencart.com">OpenCart</a><br /> %s &copy; %s';