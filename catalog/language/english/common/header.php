<?php
// Text
$_['text_home']          = 'Beranda';
$_['text_wishlist']      = 'Wish List (%s)';
$_['text_shopping_cart'] = 'Daftar Belanja';
$_['text_category']      = 'Kategori';
$_['text_account']       = 'Akun Saya';
$_['text_register']      = 'Daftar';
$_['text_login']         = 'Login';
$_['text_order']         = 'History Pesanan';
$_['text_transaction']   = 'Transaksi';
$_['text_download']      = 'Download';
$_['text_logout']        = 'Logout';
$_['text_checkout']      = 'Checkout';
$_['text_search']        = 'Cari';
$_['text_all']           = 'Lihat Semua';
