<?php
// Heading
$_['heading_title']  = 'Kontak Kami';

// Text
$_['text_location']  = 'Lokasi';
$_['text_store']     = 'Toko';
$_['text_contact']   = 'Formulir Kontak';
$_['text_address']   = 'Address';
$_['text_telephone'] = 'Telepon';
$_['text_fax']       = 'Fax';
$_['text_open']      = 'Jam Buka';
$_['text_comment']   = 'Komentar';
$_['text_success']   = '<p>Pesan anda telah berhasil dikirim kepada pemilik toko.</p>';

// Entry
$_['entry_name']     = 'Nama';
$_['entry_email']    = 'E-Mail';
$_['entry_enquiry']  = 'Pesan';

// Email
$_['email_subject']  = 'Pesan %s';

// Errors
$_['error_name']     = 'Nama harus diantara 3 sampai 32 karakter!';
$_['error_email']    = 'Alamat E-Mail tidak valid!';
$_['error_enquiry']  = 'Pesan harus diantara 10 hingga 3000 karakter!';
