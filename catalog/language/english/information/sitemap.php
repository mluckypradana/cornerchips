<?php
// Heading
$_['heading_title']    = 'Peta Situs';

// Text
$_['text_special']     = 'Penawaran Spesial';
$_['text_account']     = 'Akun Saya';
$_['text_edit']        = 'Informasi Akun';
$_['text_password']    = 'Password';
$_['text_address']     = 'Alamat';
$_['text_history']     = 'History Pemesanan';
$_['text_download']    = 'Download';
$_['text_cart']        = 'Daftar Belanja';
$_['text_checkout']    = 'Checkout';
$_['text_search']      = 'Cari';
$_['text_information'] = 'Informasi';
$_['text_contact']     = 'Kontak Kami';