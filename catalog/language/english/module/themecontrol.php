<?php 

$_['text_sale'] = 'Sale';
$_['text_new'] = 'New';
$_['text_sale_detail'] = 'Save: %s';
$_['text_contact_us'] = 'Contact Us';
$_['shop_now'] = 'Shop now';
$_['text_color_tools'] = 'Live Theme Editor';
$_['text_selectors']   = 'Layout Selectors';
$_['text_elements']   = 'Layout Elements';
$_['text_my_cart']       = 'Shopping Cart';

$_['text_sidebar_left'] = 'Sidebar Left';
$_['text_sidebar_right'] = 'Sidebar Right';
$_['quick_view']      = 'Quick View';
$_['text_about_us'] = 'About Us';
$_['text_business_hours']      = 'Business hours';
$_['text_follow']      = 'Follow us';
$_['text_item']      = 'item';
$_['text_item']      = 'item';
$_['text_items']      = 'items';
$_['text_review']      = 'Reviews';
$_['text_link'] = 'Read more';
$_['text_setting']      = 'Setting';
$_['text_account_skin']      = 'account';
$_['text_account_2kin']      = 'account';
$_['text_catalog_menu'] = 'All categories';
$_['text_shop']         = 'Shop';
$_['text_testimonial']         = 'Testimonial';
$_['text_days'] = 'Day';
$_['text_hours'] = 'Hours';
$_['text_minutes'] = 'Mins';
$_['text_seconds'] = 'Secs';
$_['text_finish'] = 'Expired';
$_['text_search_product']      = 'Search Products';
$_['text_language_currency']      = 'Language - Currency';
$_['seemore']      = 'See more';

// pav krstore

$_['text_latest']      = 'Latest';
$_['text_featured']      = 'Featured';
$_['text_bestseller']      = 'Bestseller';

?>