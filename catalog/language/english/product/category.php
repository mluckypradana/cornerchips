<?php
// Text
$_['text_refine']       = 'Ubah Pencarian';
$_['text_product']      = 'Produk';
$_['text_error']        = 'Kategori tidak ditemukan!';
$_['text_empty']        = 'Tidak ada produk pada kategori ini.';
$_['text_quantity']     = 'Qty:';
$_['text_manufacturer'] = 'Brand:';
$_['text_model']        = 'Kode Produk:';
$_['text_points']       = 'Reward Point:';
$_['text_price']        = 'Harga:';
$_['text_tax']          = 'Ex Tax:';
$_['text_compare']      = 'Perbandingan Produk (%s)';
$_['text_sort']         = 'Urut Berdasarkan:';
$_['text_default']      = 'Default';
$_['text_name_asc']     = 'Nama (A - Z)';
$_['text_name_desc']    = 'Nama (Z - A)';
$_['text_price_asc']    = 'Harga (Rendah &gt; Tinggi)';
$_['text_price_desc']   = 'Harga (Tinggi &gt; Rendah)';
$_['text_rating_asc']   = 'Rating (Lowest)';
$_['text_rating_desc']  = 'Rating (Highest)';
$_['text_model_asc']    = 'Model (A - Z)';
$_['text_model_desc']   = 'Model (Z - A)';
$_['text_limit']        = 'Tampilkan:';