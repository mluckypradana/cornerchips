<?php echo $header; ?>

<div class="container">
	<ul class="breadcrumb space-30">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		<?php } ?>
	</ul>
	<?php if ($success) { ?>
		<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
	<?php } ?>
	<div class="row">
		<?php echo $column_left; ?>
		<?php if ($column_left && $column_right) { ?>
		<?php $class = 'col-lg-6 col-md-6 col-sm-12 col-xs-12'; ?>
		<?php } elseif ($column_left || $column_right) { ?>
		<?php $class = 'col-lg-9 col-md-9 col-sm-12 col-xs-12'; ?>
		<?php } else { ?>
		<?php $class = 'col-lg-12 col-md-12 col-sm-12 col-xs-12'; ?>
		<?php } ?>
		<div class="panel-heading col-lg-12 space-20">
			<h1 class="panel-title panel-v2"><?php echo $heading_title; ?></h1>
		</div>
		<div id="content" class="<?php echo $class; ?>">
			<?php echo $content_top; ?>
			<div class="row">
				<div id="column-left" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
					<div class="panel-heading">
						<h2 class="panel-title panel-v2"><?php echo $text_my_account; ?></h2>
					</div>
					<ul class="list-unstyled panel-body space-40 space-top-10">
						<li><a href="<?php echo $edit; ?>"><?php echo $text_edit; ?></a></li>
						<li><a href="<?php echo $password; ?>"><?php echo $text_password; ?></a></li>
						<li><a href="<?php echo $address; ?>"><?php echo $text_address; ?></a></li>
						<li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
					</ul>
					<div class="panel-heading">
						<h2 class="panel-title panel-v2"><?php echo $text_my_orders; ?></h2>
					</div>
					<ul class="list-unstyled panel-body space-40 space-top-10">
						<li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
						<li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
						<?php if ($reward) { ?>
						<li><a href="<?php echo $reward; ?>"><?php echo $text_reward; ?></a></li>
						<?php } ?>
						<li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
						<li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
						<li><a href="<?php echo $recurring; ?>"><?php echo $text_recurring; ?></a></li>
					</ul>
					<div class="panel-heading">
						<h2 class="panel-title panel-v2"><?php echo $text_my_newsletter; ?></h2>
					</div>
					<ul class="list-unstyled panel-body space-40 space-top-10">
						<li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
					</ul>
					<?php echo $content_bottom; ?>
				</div>
			</div>
		</div>
		<?php echo $column_right; ?>
	</div>
</div>

<?php echo $footer; ?>