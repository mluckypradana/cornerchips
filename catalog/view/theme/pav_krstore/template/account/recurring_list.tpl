<?php echo $header; ?>
<div class="container">
	<ul class="breadcrumb space-30">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		<?php } ?>
	</ul>
	<div class="row">
		<?php echo $column_left; ?>
		<?php if ($column_left && $column_right) { ?>
		<?php $class = 'col-lg-6 col-md-6 col-sm-12 col-xs-12'; ?>
		<?php } elseif ($column_left || $column_right) { ?>
		<?php $class = 'col-lg-9 col-md-9 col-sm-12 col-xs-12'; ?>
		<?php } else { ?>
		<?php $class = 'col-lg-12 col-md-12 col-sm-12 col-xs-12'; ?>
		<?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
		<div class="panel-heading">
			<h1 class="panel-title panel-v2"><?php echo $heading_title; ?></h1>
		</div>
      <?php if ($recurrings) { ?>
      <div class="table-responsive space-top-10">
        <table class="table table-bordered table-hover">
          <thead>
            <tr>
              <td class="text-left"><?php echo $column_recurring_id; ?></td>
              <td class="text-left"><?php echo $column_date_added; ?></td>
              <td class="text-left"><?php echo $column_status; ?></td>
              <td class="text-left"><?php echo $column_product; ?></td>
              <td class="text-right"><?php echo $column_action; ?></td>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($recurrings as $recurring) { ?>
              <tr>
                <td class="text-left">#<?php echo $recurring['id']; ?></td>
                <td class="text-left"><?php echo $recurring['date_added']; ?></td>
                <td class="text-left"><?php echo $status_types[$recurring['status']]; ?></td>
                <td class="text-left"><?php echo $recurring['name']; ?></td>
                <td class="text-right"><a href="<?php echo $recurring['href']; ?>" class="btn btn-info"><?php echo $button_view; ?></a></td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
      <div class="text-right"><?php echo $pagination; ?></div>
      <?php } else { ?>
      <p class="space-top-10 space-20"><?php echo $text_empty; ?></p>
      <?php } ?>
      <div class="buttons clearfix space-30">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-v1"><?php echo $button_continue; ?></a></div>
      </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>