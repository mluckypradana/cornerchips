<?php  echo $header; ?>
<?php require( ThemeControlHelper::getLayoutPath( 'common/mass-header.tpl' )  ); ?>
<div class="main-columns container">
	<?php require( ThemeControlHelper::getLayoutPath( 'common/mass-container.tpl' )  ); ?>
	<div class="row">
		<?php if( $SPAN[0] ): ?>
			<aside id="sidebar-left" class="col-md-<?php echo $SPAN[0];?>">
				<?php echo $column_left; ?>
			</aside>	
		<?php endif; ?> 
		<div id="sidebar-main" class="col-md-<?php echo $SPAN[1];?>">
			<div id="content">
				<div class="content-inner bg-white space-40">
					<?php echo $content_top; ?>
					<div class="panel-danger panel-heading space-20"><h1 class="panel-title panel-v1"><?php echo $heading_title; ?></h1></div>
					<?php echo $description; ?>
					<?php echo $content_bottom; ?>
				</div>
			</div>
		</div>
<?php if( $SPAN[2] ): ?>
	<aside id="sidebar-right" class="col-md-<?php echo $SPAN[2];?>">	
		<?php echo $column_right; ?>
	</aside>
<?php endif; ?></div>
</div>
<?php echo $footer; ?> 