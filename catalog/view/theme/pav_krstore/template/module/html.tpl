<?php if( isset($widget_name)){
?>
<span class="menu-title"><?php echo $widget_name;?></span>
<?php
}?>
<div class="widget-html">
	<div class="widget-inner">
		<div class="panel-heading">
			<h4 class="panel-title"><?php echo $heading_title?></h4>
		</div>
		<?php echo $html; ?>
	</div>
</div>
