<?php
  $this->language( 'module/themecontrol' );
  $objlang = $this->registry->get('language');
?>
<div class="top-verticalmenu hidden-xs hidden-sm">
	<div class="menu-heading d-heading">
		<h4>
			<span class="fa fa-bars"></span>
			<?php echo $objlang->get('text_catalog_menu'); ?>              
		</h4>
	</div> 
	<div id="pav-verticalmenu"> 
		<div class="menu-content d-content">
			<div class="pav-verticalmenu fix-top">
				<div class="navbar navbar-verticalmenu">
					<div class="verticalmenu clearfix" role="navigation">
						<div class="navbar-header">
							<div class="collapse navbar-collapse navbar-ex1-collapse">
								<?php echo $treemenu; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>