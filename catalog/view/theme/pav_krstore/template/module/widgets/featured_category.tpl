<div class="featured-category row <?php echo $addition_cls?>">
	<?php if( $show_title ) { ?>
		<div class="panel-heading text-center">
			<h4 class="panel-title panel-v2 home1"><?php echo $heading_title?></h4>
		</div>
	<?php } ?>
	<?php if(!empty($categories)) { ?>
		<?php foreach ($categories as $category): ?>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 space-top-50">
				<div class="image">
					<a href="<?php echo $category['href']; ?>">
						<?php if ($category['image'] !== '') { ?>
						<img src="image/<?php echo $category['image']; ?>" alt="" class="img-responsive">
						<?php
						} ?>
					</a>
				</div>
				<div class="group-text space-top-25">
					<div class="caption text-center">
						<h6>
							<a class="btn btn-v3" href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
						</h6>
					</div>
				</div>
			</div>
		<?php endforeach ?>
	<?php } ?>
</div>