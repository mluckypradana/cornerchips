<?php $objlang = $this->registry->get('language');  $ourl = $this->registry->get('url');   ?>
<?php echo $header; ?>
<div class="container">
	<ul class="breadcrumb space-30">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		<?php } ?>
	</ul>
	<div class="row">
		<?php echo $column_left; ?>
		<?php if ($column_left && $column_right) { ?>
		<?php $class = 'col-sm-6'; ?>
		<?php } elseif ($column_left || $column_right) { ?>
		<?php $class = 'col-sm-9'; ?>
		<?php } else { ?>
		<?php $class = 'col-sm-12'; ?>
		<?php } ?>
		<div id="content" class="<?php echo $class; ?>">
			<?php echo $content_top; ?>
			<div class="criteria">
				<div class="panel-heading space-10">
					<h1 class="panel-title panel-v2"><?php echo $heading_title; ?></h1>
				</div>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<label class="control-label text-refine" for="input-search"><?php echo $entry_search; ?></label>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 space-top-20 space-20">
						<input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php $text_keyword="Cari"; echo $text_keyword; ?>" id="input-search" class="form-control" />
					</div>
					<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 space-top-20 space-20">
						<div class="select-wrap">
							<select id="category-search" name="category_id" class="form-control">
								<option value="0"><?php echo $text_category; ?></option>
								<?php foreach ($categories as $category_1) { ?>
									<?php if ($category_1['category_id'] == $category_id) { ?>
										<option value="<?php echo $category_1['category_id']; ?>" selected="selected"><?php echo $category_1['name']; ?></option>
									<?php } else { ?>
										<option value="<?php echo $category_1['category_id']; ?>"><?php echo $category_1['name']; ?></option>
									<?php } ?>
									<?php foreach ($category_1['children'] as $category_2) { ?>
										<?php if ($category_2['category_id'] == $category_id) { ?>
											<option value="<?php echo $category_2['category_id']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>
										<?php } else { ?>
											<option value="<?php echo $category_2['category_id']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>
										<?php } ?>
										<?php foreach ($category_2['children'] as $category_3) { ?>
											<?php if ($category_3['category_id'] == $category_id) { ?>
												<option value="<?php echo $category_3['category_id']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>
											<?php } else { ?>
												<option value="<?php echo $category_3['category_id']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>
											<?php } ?>
										<?php } ?>
									<?php } ?>
								<?php } ?>
							</select>
						</div>
					</div>
				</div>
				<p>
					<label class="checkbox-inline">
						<?php if ($sub_category) { ?>
							<input type="checkbox" name="sub_category" value="1" checked="checked" />
						<?php } else { ?>
							<input type="checkbox" name="sub_category" value="1" />
						<?php } ?>
						<?php echo $text_sub_category; ?>
					</label>
				</p>
				<p>
					<label class="checkbox-inline">
						<?php if ($description) { ?>
							<input type="checkbox" name="description" value="1" id="description" checked="checked" />
						<?php } else { ?>
							<input type="checkbox" name="description" value="1" id="description" />
						<?php } ?>
						<?php echo $entry_description; ?>
					</label>
				</p>
				<input type="button" value="<?php echo $button_search; ?>" id="button-search" class="btn btn-primary btn-v1 space-top-20" />
			</div>
			<div class="panel-heading space-20">
				<h2 class="panel-title panel-v1"><?php echo $text_search; ?></h2>
			</div>
			<?php if ($products) { ?>
				<div class="products-filter-panel">
					<div class="row">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="btn-group pull-left hidden-xs">
								<button type="button" id="grid-view" class="btn btn-switch active" data-toggle="tooltip" title="<?php echo $button_grid; ?>"><i class="zmdi zmdi-apps"></i></button>
								<button type="button" id="list-view" class="btn btn-switch" data-toggle="tooltip" title="<?php echo $button_list; ?>"><i class="zmdi zmdi-view-list-alt"></i></button>
							</div>
							<div class="pull-right">
								<a href="<?php echo $compare; ?>" id="compare-total"><?php echo $text_compare; ?></a>
							</div>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="pull-right">
								<div class="select-wrap">
									<select id="input-sort" class="form-control" onchange="location = this.value;">
										<?php foreach ($sorts as $sorts) { ?>
											<?php if ($sorts['value'] == $sort . '-' . $order) { ?>
												<option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
											<?php } else { ?>
												<option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
											<?php } ?>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="group-text pull-right">
								<label class="control-label" for="input-sort"><?php echo $text_sort; ?></label>
							</div>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="pull-right">
								<div class="select-wrap">
									<select id="input-limit" class="form-control" onchange="location = this.value;">
										<?php foreach ($limits as $limits) { ?>
											<?php if ($limits['value'] == $limit) { ?>
												<option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
											<?php } else { ?>
												<option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
											<?php } ?>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="group-text pull-right">
								<label class="control-label" for="input-limit"><?php echo $text_limit; ?></label>
							</div>
						</div>
					</div>
				</div>
				<div id="products" class="clearfix">
					<?php foreach ($products as $product) { ?>
						<div class="product-layout product-list col-xs-12">
							<div class="product-block">
								<?php if ($product['thumb']) {    ?>
									<div class="image">
										<?php if( $product['special'] ) {   ?>
											<span class="product-label sale-exist text-center radius-4x"><span class="product-label-special"><?php echo $objlang->get('text_sale'); ?></span></span>
										<?php } ?>
										<div class="product-img img">
											<a class="img" title="<?php echo $product['name']; ?>" href="<?php echo $product['href']; ?>">
												<img class="img-responsive" src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" />
											</a>
										</div>
									</div>
								<?php } ?>
								<div class="product-meta contents">
									<div class="top">
										<h6 class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h6>
										<?php if( isset($product['description']) ){ ?>
											<p class="description"><?php echo utf8_substr( strip_tags($product['description']),0,200);?>...</p>
										<?php } ?>
										<?php if ($product['price']) { ?>
											<div class="price clearfix">
												<?php if (!$product['special']) {  ?>
													<span class="price-olds"><?php echo $product['price']; ?></span>
													<?php if( preg_match( '#(\d+).?(\d+)#',  $product['price'], $p ) ) { ?>
													<?php } ?>
												<?php }
												else { ?>
													<span class="price-new"><?php echo $product['special']; ?></span>
													<span class="price-old"><?php echo $product['price']; ?></span>
													<?php if( preg_match( '#(\d+).?(\d+)#',  $product['special'], $p ) ) { ?> 
													<?php } ?>
												<?php } ?>
											</div>
										<?php } ?>
										<?php if ($product['rating']) { ?>
											<div class="rating">
												<?php for ($is = 1; $is <= 5; $is++) { ?>
													<?php if ($product['rating'] < $is) { ?>
														<span><i class="zmdi zmdi-star-outline"></i></span>
													<?php } else { ?>
														<span class="rate"><i class="zmdi zmdi-star-outline"></i></span>
													<?php } ?>
												<?php } ?>
											</div>
										<?php } ?>
									</div>
									<div class="bottom">
										<div class="action">
										<?php if( !isset($listingConfig['catalog_mode']) || !$listingConfig['catalog_mode'] ) { ?>
											<div class="cart">
												<button class="btn-action" data-loading-text="Loading..." type="button" onclick="cart.addcart('<?php echo $product['product_id']; ?>');">
													<i class="fa fa-shopping-cart"></i>
												</button>
											</div>
										<?php } ?>
											<div class="compare">
												<button class="btn-action" type="button" data-toggle="tooltip" data-placement="top" title="<?php echo $objlang->get("button_compare"); ?>" onclick="compare.addcompare('<?php echo $product['product_id']; ?>');"><i class="fa fa-bar-chart"></i></button> 
											</div>
											<div class="wishlist">
												<button class="btn-action" type="button" data-toggle="tooltip" data-placement="top" title="<?php echo $objlang->get("button_wishlist"); ?>" onclick="wishlist.addwishlist('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart-o"></i></button> 
											</div>
											<div class="quickview hidden-sm hidden-xs">
												<a class="iframe-link text-center btn-action quick-view" data-toggle="tooltip" data-placement="top" href="<?php echo $ourl->link('themecontrol/product','product_id='.$product['product_id']);?>"  title="<?php echo $objlang->get('quick_view'); ?>" ><i class="zmdi zmdi-eye"></i></a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php } ?>
				</div>
				<div class="paging row">
				<div class="col-sm-6 text-left space-top-10"><?php echo $results; ?></div>
				<div class="col-sm-6 text-right"><?php echo $pagination; ?></div>
				</div>
			<?php } else { ?>
				<p class="space-30"><?php echo $text_empty; ?></p>
			<?php } ?>
			<?php echo $content_bottom; ?>
		</div>
		<?php echo $column_right; ?>
	</div>
</div>

<script type="text/javascript"><!--
$('#button-search').bind('click', function() {
	url = 'index.php?route=product/search';

	var search = $('#content input[name=\'search\']').prop('value');

	if (search) {
		url += '&search=' + encodeURIComponent(search);
	}

	var category_id = $('#content select[name=\'category_id\']').prop('value');

	if (category_id > 0) {
		url += '&category_id=' + encodeURIComponent(category_id);
	}

	var sub_category = $('#content input[name=\'sub_category\']:checked').prop('value');

	if (sub_category) {
		url += '&sub_category=true';
	}

	var filter_description = $('#content input[name=\'description\']:checked').prop('value');

	if (filter_description) {
		url += '&description=true';
	}

	location = url;
});

$('#content input[name=\'search\']').bind('keydown', function(e) {
	if (e.keyCode == 13) {
		$('#button-search').trigger('click');
	}
});

$('select[name=\'category_id\']').on('change', function() {
	if (this.value == '0') {
		$('input[name=\'sub_category\']').prop('disabled', true);
	} else {
		$('input[name=\'sub_category\']').prop('disabled', false);
	}
});

$('select[name=\'category_id\']').trigger('change');
--></script>
<?php echo $footer; ?>