<?php
// HTTP
define('HTTP_SERVER', 'http://localhost/Cornerchips/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost/Cornerchips/');

// DIR
define('DIR_APPLICATION', 'D:/Xampp/htdocs/Cornerchips/catalog/');
define('DIR_SYSTEM', 'D:/Xampp/htdocs/Cornerchips/system/');
define('DIR_LANGUAGE', 'D:/Xampp/htdocs/Cornerchips/catalog/language/');
define('DIR_TEMPLATE', 'D:/Xampp/htdocs/Cornerchips/catalog/view/theme/');
define('DIR_CONFIG', 'D:/Xampp/htdocs/Cornerchips/system/config/');
define('DIR_IMAGE', 'D:/Xampp/htdocs/Cornerchips/image/');
define('DIR_CACHE', 'D:/Xampp/htdocs/Cornerchips/system/storage/cache/');
define('DIR_DOWNLOAD', 'D:/Xampp/htdocs/Cornerchips/system/storage/download/');
define('DIR_LOGS', 'D:/Xampp/htdocs/Cornerchips/system/storage/logs/');
define('DIR_MODIFICATION', 'D:/Xampp/htdocs/Cornerchips/system/storage/modification/');
define('DIR_UPLOAD', 'D:/Xampp/htdocs/Cornerchips/system/storage/upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'cornerch_ips');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
